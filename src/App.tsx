/**
 * this file is the app dom handler + Router handler.
 * If I use Redux or context in the future, the providers are going to be added to the project
 * from here.
 * Inputs: routes and state manager provider/s
 * output: The whole project.
 */

import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from 'react-redux';
import Home from './pages/Home';
import Done from './pages/done';
import store from './Redux/store';


function App() {
  return (
    <Provider store={store}>
      <Router>
        <div>
          <Route exact path='/' component={Home} />
          <Route path='/done' component={Done} />
        </div>
      </Router>
    </Provider>
  );
}

export default App;
