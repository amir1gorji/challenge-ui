import { createStore } from 'redux';
import reducers from './reducers';
import throttle from 'lodash/throttle'
import { loadState, saveState } from './functions/preserveData';

// loadState checks is any redux state is saved in the sessionStorage.
// so everytime we execute this function it returns an object of saved redux state in the sessionStorage
const persistedState: any = loadState();

// persisted state is a function execution that gets invoked when the store is being made.
// so when the redux is making the store, it checks if there is any saved state on the sessionStorage
const store = createStore(reducers, persistedState);


// this function saves the current state of the redux in the browserSession storage
// method subscribe is a reactive method which is being called eveytime the state gets changed
// because savestate function uses JSON.stringify() method which is a cpu consuming process
// we want to make sure that saveState doesn't lower the app performance so we need to make sure
// it's being called once a second, so I used lodash throttle.
store.subscribe(throttle(() => {
  saveState(store.getState())
}, 1000));

export default store;