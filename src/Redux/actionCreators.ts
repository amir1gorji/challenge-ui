/** this is the action creator
 * any actions dispatched to the redux are declared here
 */

import { ts_type, endDateType, } from "../models/typescript/actionTypes/reduxActionTypes"
import { ts_redux_schedule_repeats } from "../models/typescript/reduxState"

///////  ***** schedule actions  *****  ///////
export function setDate(date: Date | string) {
  return { type: ts_type.SET_DATE, payload: date }
}

export function setMeetingType(meetingType: string) {
  return { type: ts_type.SET_MEETING_TYPE, payload: meetingType }
}

export function setRoomType(roomType: string) {
  return { type: ts_type.SET_ROOM_TYPE, payload: roomType }
}

export function setRepeat(repeats: string) {
  return { type: ts_type.SET_REPEAT, payload: repeats }
}

export function setRepeatsOn(repeats: ts_redux_schedule_repeats) {
  return { type: ts_type.SET_REPEATS_ON, payload: repeats }
}

export function setRepeatEveryNumber(repeats: number) {
  return { type: ts_type.SET_REPEAT_EVERY_NUMBER, payload: repeats }
}

export function setRepeatEveryFrequency(repeats: number) {
  return { type: ts_type.SET_REPEAT_EVERY_FREQUENCY, payload: repeats }
}

export function setRecurrence(repeats: string) {
  return { type: ts_type.SET_RECURRENCE, payload: repeats }
}

export function setEndDateType(endDateType: endDateType) {
  return { type: ts_type.SET_END_TYPE, payload: endDateType }
}
///////   schedule actions    ///////

///////  ***** attendees actions  *****  ///////
export function addAttendee(name: string) {
  return { type: ts_type.ADD_ATTENDEE, payload: name }
}

export function removeAttendee(name: string) {
  return { type: ts_type.REMOVE_ATTENDEE, payload: name }
}
///////   attendees actions    ///////
