import { TS_Redux_state } from "../../models/typescript/reduxState";

export const loadState = (): undefined | string => {
    try {
        const serializedState = sessionStorage.getItem('state');
        if (serializedState === null)
            return undefined
        // a string is saved in sessionStorage, we must parse it as an object
        return JSON.parse(serializedState)
    } catch (err) {
        return undefined
    }
}

export const saveState = (state: TS_Redux_state) => {
    try {
        // any data must be saved on the sessionStorage as a string
        const serializedState = JSON.stringify(state);
        sessionStorage.setItem('state', serializedState);
    } catch (err) {
        //do nothing for now
    }
}