import { combineReducers } from "redux";
import attendees from "./attendees";
import schedule from "./schedule";


export default combineReducers({
  attendees,
  schedule
})