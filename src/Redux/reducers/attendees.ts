/** attendees Reducer
 * any changes related to object attendees in the redux store is handled here
 */
import { ts_type } from "../../models/typescript/actionTypes/reduxActionTypes";
import { ts_redux_state_attendees } from "../../models/typescript/reduxState";

let initialState: ts_redux_state_attendees = {
  names: []
}


export default (state = initialState, action: any) => {
  switch (action.type) {
    case ts_type.REMOVE_ATTENDEE: {
      //finds the index of the item that must be deleted
      let toRemoveIndex = state.names.findIndex(item => item === action.payload);
      // then deletes that item
      delete (state.names[toRemoveIndex]);
      // but the item is not completely deleted, the array item is now empty, to completely delete it
      // we use array.dilter method
      state.names = state.names?.filter((x: string) => x);
      return { ...state, date: action.payload };
    }
    case ts_type.ADD_ATTENDEE: {
      state.names.push(action.payload);
      return { ...state, meetingType: action.payload };
    }
    default: return state;
  }
}