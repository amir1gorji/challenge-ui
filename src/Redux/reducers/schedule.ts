/** schedule Reducer
 * any changes related to object schedule in the redux store is handled here
 */

import { ts_type, endDateType } from "../../models/typescript/actionTypes/reduxActionTypes";
import { ts_redux_state_schedule } from "../../models/typescript/reduxState";

let initialState: ts_redux_state_schedule = {
  date: new Date().toISOString(),
  meetingType: 'Legaler Meeting',
  roomType: 'private',
  repeatsOn: {
    saturday: false,
    sunday: false,
    monday: false,
    tuesday: false,
    wednesday: false,
    thursday: false,
    friday: false
  },
  repeatEveryFrequency: 'Week',
  repeatEveryNumber: 1,
  repeats: 'Repeats',
  endDateType: 'none',
  recurrence: '1 Concurrence',
}


export default (state = initialState, action: any) => {
  switch (action.type) {
    case ts_type.SET_DATE: return { ...state, date: action.payload };
    case ts_type.SET_MEETING_TYPE: return { ...state, meetingType: action.payload };
    case ts_type.SET_ROOM_TYPE: return { ...state, roomType: action.payload };
    case ts_type.SET_REPEAT: return { ...state, repeats: action.payload };
    case ts_type.SET_REPEAT_EVERY_NUMBER: return { ...state, repeatEveryNumber: action.payload };
    case ts_type.SET_REPEAT_EVERY_FREQUENCY: return { ...state, repeatEveryFrequency: action.payload };
    case ts_type.SET_RECURRENCE: return { ...state, recurrence: action.payload };
    case ts_type.SET_REPEATS_ON: return { ...state, repeatsOn: action.payload }; //TODO: to be done
    case ts_type.SET_END_TYPE: return { ...state, endDateType: action.payload }; //TODO: to be done
    default: return state;
  }
}