import { endDateType } from "./actionTypes/reduxActionTypes";

export interface TS_Redux_state {
  schedule: ts_redux_state_schedule
  attendees: ts_redux_state_attendees
}

export interface ts_redux_state_schedule {
  meetingType: string;
  roomType: string;
  repeats: string;
  repeatEveryNumber: number;
  repeatEveryFrequency: string;
  repeatsOn: ts_redux_schedule_repeats;
  date: Date | string;
  endDateType: endDateType | 'none';
  recurrence: string;
}

export interface ts_redux_state_attendees {
  names: string[];
}

export interface ts_redux_schedule_repeats {
  monday: boolean;
  tuesday: boolean;
  wednesday: boolean;
  thursday: boolean;
  friday: boolean;
  saturday: boolean;
  sunday: boolean;
}
