export enum ts_type {
  SET_DATE = 'SET_DATE',
  SET_ROOM_TYPE = 'SET_ROOM_TYPE',
  SET_REPEAT = 'SET_REPEAT',
  SET_MEETING_TYPE = 'SET_MEETING_TYPE',
  SET_REPEAT_EVERY_NUMBER = 'SET_REPEAT_EVERY_NUMBER',
  SET_REPEAT_EVERY_FREQUENCY = 'SET_REPEAT_EVERY_FREQUENCY',
  SET_RECURRENCE = 'SET_RECURRENCE',
  SET_REPEATS_ON = 'SET_REPEATS_ON',
  SET_END_TYPE = 'SET_END_TYPE',
  // attendees action types
  ADD_ATTENDEE = 'ADD_ATTENDEE',
  REMOVE_ATTENDEE = 'REMOVE_ATTENDEE',
}

export enum endDateType {
  by = 'by',
  after = 'after'
}

export enum weekDay {
  saturday = 'saturday',
  sunday = 'sunday',
  monday = 'monday',
  tuesday = 'tuesday',
  wednesday = 'wednesday',
  thursday = 'thursday',
  friday = 'friday',
}
