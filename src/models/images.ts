export let IMAGE_profile = `${process.env.PUBLIC_URL}/assets/images/profile.png`

export default {
  IMAGE_profile
}
