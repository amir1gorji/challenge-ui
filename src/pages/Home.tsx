/**
 * Home screen will be shown to the user once they open the web app
 * any interactions on the app will be done here
 * there's another web page named done
 */

import React from 'react';
import { useDispatch } from 'react-redux';
import { AttendeesSelector, MyDatePicker, OptionSelector, WeekdayItem } from '../components';
import { endDateType, weekDay } from '../models/typescript/actionTypes/reduxActionTypes';
import { setMeetingType, setRepeat, setRepeatEveryNumber, setRoomType, setRepeatEveryFrequency, setRecurrence, setEndDateType } from '../Redux/actionCreators';

export interface HomeProps {
  history: { push: Function }
}


const Home: React.FunctionComponent<HomeProps> = ({ history }) => {

  let dispatch = useDispatch();

  // after changing the endDateType, we must change the raio buttons' check states
  // and dispatch this new data to the redux (it's only a string "by" or "after")
  function checkAfterRadioButton() {
    let by: any = document.querySelector('#by');
    let after: any = document.querySelector('#after');
    by.checked = false;
    after.checked = true;
    dispatch(setEndDateType(endDateType.after));
  }


  return (
    <div>
      <div className="md:flex">
        <div className="p-6 bg-gray-white md:w-1/2 h-screen">
          <h1 className="text-gray-800 font-sans text-lg">Schedule Meeting</h1>
          <div className="flex items-center w-full border-b border-gray-400 px-2 py-2 mt-5">
            <input className="appearance-none border-none bg-transparent focus:outline-none" type="text"
              placeholder="Add Meeting Subject" />
          </div>
          <div className="flex items-center mt-8">
            Icon
            Attendees
      </div>
          <AttendeesSelector />

          <h2 className="text-xl mt-4">Meeting type</h2>
          <div className="flex mt-4">
            <OptionSelector reduxState='meetingType' actionCreator={setMeetingType} options={['Leagaler Meeting', 'Amir\'s Meeting', 'none']} />
            <OptionSelector reduxState='roomType' actionCreator={setRoomType} options={['Private', 'Group', 'Public']} containerClass='ml-4' />
          </div>

          <OptionSelector reduxState={'repeats'} actionCreator={setRepeat} options={['Repeats', 'Daily', 'Weekly', 'Monthly']} containerClass='mt-4' />


          <div className="flex items-center mt-4">
            <div className="text-gray-400">Repeats every</div>
            <OptionSelector reduxState={'repeatEveryNumber'} actionCreator={setRepeatEveryNumber} options={['1', '2', '3', '4', '5', '6', '7']} containerClass='ml-6' />
            <OptionSelector reduxState={'repeatEveryFrequency'} actionCreator={setRepeatEveryFrequency} options={['Week', 'Month', 'Year']} containerClass='ml-6' />
          </div>

          <div className="flex items-center mt-4">
            <div className="text-gray-400 mr-6">Repeats every</div>
            <WeekdayItem weekDay={weekDay.sunday}>S</WeekdayItem>
            <WeekdayItem weekDay={weekDay.monday}>M</WeekdayItem>
            <WeekdayItem weekDay={weekDay.tuesday}>T</WeekdayItem>
            <WeekdayItem weekDay={weekDay.wednesday}>W</WeekdayItem>
            <WeekdayItem weekDay={weekDay.thursday}>T</WeekdayItem>
            <WeekdayItem weekDay={weekDay.friday}>F</WeekdayItem>
            <WeekdayItem weekDay={weekDay.saturday}>S</WeekdayItem>
          </div>

          <div className="flex items-center mt-4 flex-wrap">
            <div className="text-gray-400 mr-6">End Date</div>
            <input type="radio" name="endTime" value="By" id="by" className={'mr-2'} />
            <label htmlFor={'by'} >By</label>
            <div>
              <MyDatePicker />
            </div>

            <div className="flex items-center ml-4">
              <input type="radio" name="endTime" value="After" id="after" />
              <label htmlFor={'after'}>After</label>
              <OptionSelector onSelect={checkAfterRadioButton} reduxState={'recurrence'} actionCreator={setRecurrence} options={['1 Recurrence', '2 Recurrences', '3 Recurrences', '4 Recurrences', '5 Recurrences']} containerClass='ml-2' />
            </div>
          </div>
        </div>

        <div className="p-6 bg-gray-100 md:w-1/2 h-screen">
          <button
            className="bg-transparent hover:bg-blue-500 text-blue-700 font-semibold hover:text-white py-2 px-4 border border-blue-500 hover:border-transparent rounded"
            onClick={() => history.push('done')}>
            Submit
          </button>
        </div>
      </div>
    </div >
  );
}

export default Home;