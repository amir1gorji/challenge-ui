/**
 * The page "done"
 * this page only shows the data on the redux
 * input: only from redux
 * actions: neither no actions nor any user interaction
 * output: showing data
 */
import React from 'react';
import { useSelector } from 'react-redux';
import { weekDay } from '../models/typescript/actionTypes/reduxActionTypes';
import { TS_Redux_state } from '../models/typescript/reduxState';

export interface ScheduledProps {

}

const Done: React.SFC<ScheduledProps> = () => {

  const { attendees, schedule } = useSelector(({ attendees, schedule }: TS_Redux_state) => ({
    attendees,
    schedule
  }));


  return (
    <div>
      <div className="m-6">
        <p>{attendees.names}</p>
      </div>
      <div className="m-6">
        <p>
          date: {schedule.date}
        </p>
        <p>
          endDateType: {schedule.endDateType}
        </p>
        <p>
          meetingType: {schedule.meetingType}
        </p>
        <p>
          recurrence: {schedule.recurrence}
        </p>
        <p>
          repeatEveryFrequency: {schedule.repeatEveryFrequency}
        </p>
        <p>
          repeatEveryNumber: {schedule.repeatEveryNumber}
        </p>
        <p>
          repeats: {schedule.repeats}
        </p>
        <div>
          <p>repeats on:</p>
          <p>{weekDay.monday}: {schedule.repeatsOn.monday ? 'yes' : 'no'}</p>
          <p>{weekDay.tuesday}: {schedule.repeatsOn.tuesday ? 'yes' : 'no'}</p>
          <p>{weekDay.wednesday}: {schedule.repeatsOn.wednesday ? 'yes' : 'no'}</p>
          <p>{weekDay.thursday}: {schedule.repeatsOn.thursday ? 'yes' : 'no'}</p>
          <p>{weekDay.friday}: {schedule.repeatsOn.friday ? 'yes' : 'no'}</p>
          <p>{weekDay.saturday}: {schedule.repeatsOn.saturday ? 'yes' : 'no'}</p>
          <p>{weekDay.sunday}: {schedule.repeatsOn.sunday ? 'yes' : 'no'}</p>
        </div>
        <p>roomType: {schedule.roomType}</p>
      </div>
    </div>
  );
}

export default Done;