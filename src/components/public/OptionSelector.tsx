/**
 * OptionSelector
 * input: redux actions as plain object
 * action: invoking redux using input actions
 * needs no data from redux, but I wrote the code in case I needed some redux data
 */

import React from 'react';
import { useDispatch } from 'react-redux';

export interface OptionSelectorProps {
  containerClass?: string;
  selectClass?: string;
  options: string[];
  reduxState: string;
  actionCreator: Function;
  onSelect?: Function;
}

export const OptionSelector: React.FunctionComponent<OptionSelectorProps> = ({ containerClass, selectClass, options, reduxState, actionCreator, onSelect }) => {


  let dispatch = useDispatch();
  // const state = useSelector((state: any) => ({
  //   [reduxState]: state[reduxState],  // we only need a date, so we export it like this
  // }));


  return (
    <form >
      <div className={`inline-block relative w-auto ${containerClass}`}>
        <select onChange={(event) => {
          console.log('event: ', event.target.value);
          // dispatch the active status of the weekday to redux
          dispatch(actionCreator(event.target.value));
          if (onSelect) // fire the callback function if it exists
            onSelect();
        }}
          className={`block appearance-none w-full bg-gray-300 hover:border-gray-500 px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline ${selectClass}`}>
          {/* list the menu items */}
          {options?.map((optionItem: string, index: number) => <option key={index + ''} value={optionItem}>{optionItem}</option>)}
        </select>
        <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center text-gray-700">
          <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
            <path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z" /></svg>
        </div>
      </div>
    </form >
  );
}
