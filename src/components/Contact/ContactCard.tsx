/**
 * ContactCard is a a card item.
 * input is a click interaction deleting itself from the list.
 * it's being used on the home page inside another component named <AttendeesSelector/>
 * the prop "name" is an item of the redux state.attendees.names
 */

import React from 'react';
import { IMAGE_profile } from '../../models/images';
import { useDispatch, } from "react-redux";
import { removeAttendee } from '../../Redux/actionCreators';

export interface ContactCardProps {
  name: string;
}

export const ContactCard: React.FunctionComponent<ContactCardProps> = ({ name }) => {
  //using useDispatch makes us able to dispatch an action into the redux store
  const dispatch = useDispatch();

  return (
    <div
      className="mt-4 w-auto h-16 border rounded-md flex items-center p-4"
      onClick={() => {
        dispatch(removeAttendee(name)); // triggers an item delete function
      }}
    >
      <div>||</div>
      <img alt='profile' src={IMAGE_profile} className="ml-2 h-12 w-12 rounded-full border-blue-500 border-2" />
      <div className="text-left m-2">
        <h2 className="text-black">{name}</h2>
        <div className="text-gray-500 text-sm">Perth, Australia (GMT +8) </div>
      </div>
    </div>
  );
}

