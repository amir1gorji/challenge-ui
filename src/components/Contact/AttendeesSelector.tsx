/**
 * AttendeesSelector is a component which consists of a textInput which has a redux action
 * a list of cards, each one is an item of the array name, which is a property of attendees propery
 * of redux state
 */
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TS_Redux_state } from '../../models/typescript/reduxState';
import { addAttendee } from '../../Redux/actionCreators';
import { ContactCard } from './ContactCard';

export interface AttendeesSelectorProps { }

export const AttendeesSelector: React.FunctionComponent<AttendeesSelectorProps> = () => {

  //using useDispatch makes us able to dispatch an action into the redux store
  const dispatch = useDispatch();

  // useState makes the functionalComponents able to have state.
  let [inputText, setInputText] = React.useState<string>('');


  // The less data you pass from the redux selector, the less unintentional refresh happens
  const { list } = useSelector((state: TS_Redux_state) => ({
    list: state.attendees.names,  // we only need a list of attendees, so we export it like this
  }));

  return (
    <>
      <input
        value={inputText}
        onChange={(event) => { setInputText(event.target.value) }}
        onKeyDown={function (e) {
          // if the user presses Enter on the keyboard after typing the name or the email of a contact
          // it's actually an onSubmit without implicitly declaring a submit button.
          if (e.key === 'Enter') {
            dispatch(addAttendee(inputText));
          }
        }}
        className="bg-gray-100 h-10 mt-4 px-2 w-full" placeholder="Add Name or invite by email" />
      {list?.map((name, index) => <ContactCard name={name} key={index + ''} />)}
    </>
  );
}
