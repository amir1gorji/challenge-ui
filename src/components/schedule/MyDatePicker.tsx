/**
 * My Date Picker is a date picker component that takes only user action
 * and stores the chosen date to the redux.
 */

import React, { InputHTMLAttributes, useState } from "react";
import { DatePicker } from 'react-rainbow-components/components/'
import { useDispatch, useSelector } from "react-redux";
import { endDateType } from "../../models/typescript/actionTypes/reduxActionTypes";
import { TS_Redux_state } from "../../models/typescript/reduxState";
import { setDate, setEndDateType } from "../../Redux/actionCreators";

export let MyDatePicker = (props: unknown) => {

  let dispatch = useDispatch();
  const { reduxDate } = useSelector((state: TS_Redux_state) => ({
    reduxDate: state.schedule.date,  // we only need a date, so we export it like this
  }));

  let setReduxDate = (date: Date | string) => {
    dispatch(setEndDateType(endDateType.after));
    dispatch(setDate(date));
    console.log({ date });
  }

  // making a state for date and giving in initial value of now in string form.
  let [datePicked, setDatePicked] = useState<Date | string>(new Date().toISOString());

  // every time we choose a time, this function will be called
  let onchange = (result: Date) => {
    // changin the component state date
    setDatePicked(result);
    // dispatching an action which changes the due date in the redux
    setReduxDate(result);


    let by: any = document.querySelector('#by');

    let after: any = document.querySelector('#after');
    //  checks the radio button "By"
    by.checked = true;
    // unchecks the radio button "After"
    after.checked = false;
  }

  return (
    <DatePicker
      value={datePicked}
      onChange={onchange}
      className='ml-2'
    />
  );
}