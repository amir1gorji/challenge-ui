/**
 * Weekday item
 * iputs: a string named weekDay to send the weekDay name to redux action creator
 * making the component able to track which weekday must be changed and dispatched to redux 
 */

import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { weekDay } from '../../models/typescript/actionTypes/reduxActionTypes';
import { TS_Redux_state } from '../../models/typescript/reduxState';
import { setRepeatsOn } from '../../Redux/actionCreators';

export interface WeekdayItemProps {
  children: any;
  weekDay: weekDay
}

export const WeekdayItem: React.FunctionComponent<WeekdayItemProps> = ({ children, weekDay }) => {

  let [active, setActive] = useState<boolean>(false);

  // redux action dispatcher hook
  let dispatch = useDispatch();

  // taking the data from redux store
  const { reduxRepeatsOn } = useSelector((state: TS_Redux_state) => ({
    reduxRepeatsOn: state.schedule.repeatsOn,  // we only need a list of attendees, so we export it like this
  }));

  // change the weekday items in the weekdays object 
  // toggles the active status on the component state
  // then dispatches the reduxRepeatsOn WeekDays with the new object
  let toggleActive = () => {
    reduxRepeatsOn[weekDay] = !active;
    setActive((action) => !action);
    dispatch(setRepeatsOn(reduxRepeatsOn));
  }

  return (
    <div
      className={`h-8 w-8 rounded-md ${active ? 'bg-blue-500 text-white' : 'bg-gray-300 text-black '} mr-2 flex items-center justify-center cursor-pointer`} onClick={toggleActive}>{children}</div>
  );
}
