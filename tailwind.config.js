module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    // purgeLayersByDefault: true,
  },
  theme: {
    extend: {},
  },
  purge: {
    enabled: true,
    content: ['./src/**/*.html'],
    content: ['./src/**/*.jsx'],
    content: ['./src/**/*.tsx'],
  },
  variants: {},
  plugins: [],
}
    // TODO: performance optimiztions yet to be done